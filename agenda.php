<?php
require __DIR__ . '/vendor/autoload.php';
require './Services/helper.php';
$m='';
$id_event='';
$link_event='';
if (isset($_POST))
    {
    	  $action ='addEvent';
      
     $client = getClient();
      $calendarService = new Google_Service_Calendar($client);
      $calendarId = 'primary';
      $contenido= "atender a: \n".$_POST['nombre']." ".$_POST['apellido']."\n telefono: ".$_POST['tel']."\n correo: ".$_POST['correo'];
      $fechaInicio = $_POST['fecha'].':00-00:00';
       $fecha_Fin = $_POST['fecha_fin'];
      $optParams = array(
      'maxResults' => 10,
      'orderBy' => 'startTime',
      'singleEvents' => true,
      'timeMin' => date('c'),
                      );
      $results = $calendarService->events->listEvents($calendarId, $optParams); 
      $events = $results->getItems();
      $calendarService = new Google_Service_Calendar($client);

  switch ($action) {
      case 'addEvent':
      
          $event = new Google_Service_Calendar_Event(array(
              'summary' => 'Agenda pintura',
              //codigo postal
              'location' => '76117' ,
                  //total de preguntas
              'description' => "pintar una casa, a".$contenido ,
              // "background"=> '#2FF50E',
              // "foreground"=> '#46ADAB',       
              'start' => array(                
                  'dateTime' => $fechaInicio,
                  'timeZone' => 'America/Mexico_City',
              ),
              'end' => array(
                  'dateTime' =>  $fecha_Fin,
                  'timeZone' => 'America/Mexico_City',
              ),
              "conferenceData" => [
                  "createRequest" => [
                      "conferenceId" => [
                          "type" => "eventNamedHangout"
                      ],
                      "requestId" => "123"
                  ]
              ],
              'recurrence' => array(
                  'RRULE:FREQ=DAILY;COUNT=1'
              ),
              'attendees' => array(
                  //correo adminitrador / cliente
              array('email' => $_POST['correo']),
  /*            array('email' => $email),*/
              
              array('email' => 'handyhogar@gmail.com'),
              ),
              'reminders' => array(
                  'useDefault' => FALSE,
                  'overrides' => array(
                      array('method' => 'email', 'minutes' => 24 * 60),
                      array('method' => 'popup', 'minutes' => 10),
                  ),
              ),
          ));

          $calendarId = 'primary';
          $event = $calendarService->events->insert($calendarId, $event, ['conferenceDataVersion' => 1]);
          printf('Event created: %s\n', $event->getHangoutLink());
          break;
      case 'listEvents':
          print "Upcoming events:\n";
          foreach ($events as $event) {
              $start = $event->start->dateTime;
              if (empty($start)) {
                  $start = $event->start->date;
              }
              printf("%s (%s)\n", $event->getSummary(), $start);
          }
          break;
      default:
          printf('Its not possible to get the events');
          break;
          }

        if ($events > 0) {
        } else {
          }
      }

?>